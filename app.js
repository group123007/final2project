const express = require('express')
const app = express()
const port = 8000

app.get('/', (req, res) => {
    res.send('This is a simple node js project by asavari ')
})

app.listen(port, () => {
    console.log('Application is listening at http://localhost:${port}')
})